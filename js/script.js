const btn = document.querySelector('button');
const div = document.querySelector('div');
//1

btn.addEventListener('click', () => {
  setTimeout(() => {
    div.textContent = 'Successfully changed :)';
  }, 3_000);
});

//2
// btn.addEventListener('click', () => {
//   let index = 10;
//   div.textContent = index;

//   const interval = setInterval(() => {
//     index--;
//     div.textContent += " " + index;
    
//     if(index === 0){
//       clearInterval(interval);
//       div.textContent = 'Зворотній відлік завершено';
//     }
//     console.log(div.textContent);
//   }, 1_000);
// });